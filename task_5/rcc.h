#ifndef RCC_H
#define RCC_H

#include "stm32f4xx.h"

//  +-------------------------------------+
//  |                 RCC                 |
//  +-------------------------------------+
#define PLL_M   4
#define PLL_N   168
#define PLL_P   2
#define PLL_Q   7

void rcc_reset()
{
    SET_BIT(RCC->CR, RCC_CR_HSION);
    while (RCC->CR, RCC_CR_HSIRDY == RESET);
    
    CLEAR_REG(RCC->CFGR);
    while (READ_BIT(RCC->CFGR, RCC_CFGR_SWS) != RESET);

    CLEAR_BIT(RCC->CR, RCC_CR_PLLON);
    while (READ_BIT(RCC->CR, RCC_CR_PLLRDY) != RESET);

    CLEAR_BIT(RCC->CR, RCC_CR_HSEON | RCC_CR_CSSON);
    while (READ_BIT(RCC->CR, RCC_CR_HSERDY) != RESET);

    CLEAR_BIT(RCC->CR, RCC_CR_HSEBYP);

    //Reset all CSR flags
    SET_BIT(RCC->CSR, RCC_CSR_RMVF);
    //Disable all interrupts
    CLEAR_REG(RCC->CIR);
}

int rcc_init()
{
    RCC->AHB1ENR|=RCC_AHB1ENR_GPIOCEN;
	GPIOC->MODER&=~GPIO_MODER_MODER9;
	GPIOC->MODER|=GPIO_MODER_MODER9_1;
	GPIOC->OSPEEDR|=GPIO_OSPEEDER_OSPEEDR9;
	RCC->CFGR|=RCC_CFGR_MCO2PRE;

    RCC->CR |= RCC_CR_HSEON; 

    for(int start_up_counter = 0; start_up_counter < 50; start_up_counter++)
    {
        if(RCC->CR & RCC_CR_HSEON)
            break;

        if(start_up_counter > 40)
        {
            RCC->CR &= ~RCC_CR_HSEON;
            return 1;
        }
    }

    RCC->CFGR   |=  RCC_CFGR_HPRE_DIV1
                |   RCC_CFGR_PPRE2_DIV2
                |   RCC_CFGR_PPRE1_DIV4;

    RCC->PLLCFGR = PLL_M
                    |(PLL_N<<6)
                    |(((PLL_P>>1)-1)<<16)
                    |RCC_PLLCFGR_PLLSRC_HSE|
                    (PLL_Q<<24);

    RCC->CR |= RCC_CR_PLLON;
	while (!(RCC->CR &RCC_CR_PLLRDY));

    FLASH->ACR  |=  FLASH_ACR_ICEN 
                |   FLASH_ACR_DCEN
                |   FLASH_ACR_LATENCY_5WS
                |   FLASH_ACR_PRFTEN;

    RCC->CFGR &= ~RCC_CFGR_SW;
    RCC->CFGR |= RCC_CFGR_SW_PLL;

    while (!(RCC->CFGR & RCC_CFGR_SWS ));
    return 0;
}

void SysTick_Init(void)
{
  MODIFY_REG(SysTick->LOAD,SysTick_LOAD_RELOAD_Msk, 168000000 / 1000 - 1);
  CLEAR_BIT(SysTick->VAL, SysTick_VAL_CURRENT_Msk);
  SET_BIT(SysTick->CTRL, SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk);
}
#endif 