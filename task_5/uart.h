#ifndef UART_H

#define UART_H

#include "stm32f4xx.h"
#include <string.h>
#include "seven_seg.h"

void uart3_write_2byte(int16_t data);

//  +-------------------------------------+
//  |                 UART 6              |
//  +-------------------------------------+
/* 
* TX -> PG14
* RX -> PG9
*/

void uart_6_init()
{
    RCC->AHB1ENR  |= RCC_AHB1ENR_GPIOGEN;
    //TX
    GPIOG->MODER   |= GPIO_MODER_MODER14_1;
    GPIOG->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR14; 
    GPIOG->OTYPER  &= ~(1 << 14); 
    GPIOG->AFR[1]  |= (8 << 24); 

    //RX
    GPIOG->MODER   |= GPIO_MODER_MODER9_1;
    GPIOG->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9; 
    GPIOG->OTYPER  |= (1 << 9); 
    GPIOG->AFR[1]  |= (8 << 4);    

    RCC->APB2ENR |= RCC_APB2ENR_USART6EN;

    USART6->BRR =(84000000+115200/2)/115200;
    USART6->CR1 = 0;
    USART6->CR1 |= USART_CR1_TE | USART_CR1_RE| USART_CR1_RXNEIE;
        
    USART6->CR1 |= USART_CR1_UE;    
}

 void usart_6_write_char(char data)
{
    while (!(USART6->SR & USART_SR_TXE)) {};
    USART6->DR = data;

}

void uart_6_write_2_bytes(uint16_t data)
{
    usart_6_write_char((char)(data >> 8));
    usart_6_write_char((char)data);
}

void usart_6_write_str(const char * data)
{
	char c;
	while((c=*data ++)){ usart_6_write_char(c);}
}
//--------------------------------------------------
void uart_3_init()
{
    RCC->AHB1ENR  |= RCC_AHB1ENR_GPIOBEN;
    //TX
    GPIOB->MODER   |= GPIO_MODER_MODER10_1;
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR10; 
    GPIOB->OTYPER  &= ~(1 << 10); 
    GPIOB->AFR[1]  |= (7 << 8); 

    //RX
    GPIOB->MODER   |= GPIO_MODER_MODER11_1;
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR11; 
    GPIOB->OTYPER  |= (1 << 11); 
    GPIOB->AFR[1]  |= (7 << 12); 

    RCC->APB1ENR |= RCC_APB1ENR_USART3EN;

    USART3->BRR =(42000000+115200/2)/115200;
    USART3->CR1 = 0;
    USART3->CR1 |= USART_CR1_TE;        // transmitter enable
    USART3->CR1 |= USART_CR1_RE;        // receiver enable
    USART3->CR1 |= USART_CR1_RXNEIE;    // RXNE interrupt enable
    //USART3->CR1 |= USART_CR1_TXEIE;     // TXE interrupt enable

    NVIC_EnableIRQ(USART3_IRQn);  
    SET_BIT(USART3->CR3, USART_CR3_EIE);  
    

    USART3->CR1 |= USART_CR1_UE;  
    
}

 void uart3_write_char(char data)
{
    while (!(USART3->SR & USART_SR_TXE));
    USART3->DR = data;
}

void uart3_write_2byte(int16_t data)
{
    uart3_write_char((data >> 8));
    uart3_write_char(data);
}


void uart3_write_str(char* data)
{
     uint16_t size = strlen (data);      //size is uint16_t type. What will be if size = 256 ?
    for (char i = 0; i < size; ++i)      // The variable 'i' will always be less than 256.
    {                                    // We will never exit the loop.
        if (i > 4)                       // In our case, the size is always less than 5 characters (9999 + \n)
            break;
        uart3_write_char(data[i]);
    }

}

#endif
