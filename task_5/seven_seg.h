#ifndef SEVEN_SEG_H
#define SEVEN_SEG_H

#include "stm32f4xx.h"
#include "spi_2.h"

int16_t convert_to_seven_segemnt(int8_t number)
{
    int16_t seven_seg_data = 0;
    seven_seg_data |= (1<<10)|(1<<9)|(1<<8)|(1<<0); // set all butons
    switch (number)
    {
    case 0:
        {               //   |  A   |  B   |   C   |   D   |   E   |   F  |   G   |
            seven_seg_data |= (1<<3)|(1<<7)|(1<<14)|(1<<12)|(1<<11)|(1<<4)|(0<<15);
            return seven_seg_data;
        }
    case 1:
        {
            seven_seg_data |= (0<<3)|(1<<7)|(1<<14)|(0<<12)|(0<<11)|(0<<4)|(0<<15);
            return seven_seg_data;
        }  
    case 2:
        {
            seven_seg_data |= (1<<3)|(1<<7)|(0<<14)|(1<<12)|(1<<11)|(0<<4)|(1<<15);
            return seven_seg_data;
        }  
    case 3:
        {
            seven_seg_data |= (1<<3)|(1<<7)|(1<<14)|(1<<12)|(0<<11)|(0<<4)|(1<<15);
            return seven_seg_data;
        }      
    case 4:
        {
            seven_seg_data |= (0<<3)|(1<<7)|(1<<14)|(0<<12)|(0<<11)|(1<<4)|(1<<15);
            return seven_seg_data;
        }  
    case 5:
        {               //   |  A   |  B   |   C   |   D   |   E   |   F  |   G   |
            seven_seg_data |= (1<<3)|(0<<7)|(1<<14)|(1<<12)|(0<<11)|(1<<4)|(1<<15);
            return seven_seg_data;
        }      
    case 6:
        {
            seven_seg_data |= (1<<3)|(0<<7)|(1<<14)|(1<<12)|(1<<11)|(1<<4)|(1<<15);
            return seven_seg_data;
        }  
    case 7:
        {
            seven_seg_data |= (1<<3)|(1<<7)|(1<<14)|(0<<12)|(0<<11)|(0<<4)|(0<<15);
            return seven_seg_data;
        }  
    case 8:
        {
            seven_seg_data |= (1<<3)|(1<<7)|(1<<14)|(1<<12)|(1<<11)|(1<<4)|(1<<15);
            return seven_seg_data;
        }    
    case 9:
        {
            seven_seg_data |= (1<<3)|(1<<7)|(1<<14)|(1<<12)|(0<<11)|(1<<4)|(1<<15);
            return seven_seg_data;
        }  
    default:
        return seven_seg_data;
    }
}

uint16_t wait_time = 5;
void show_number(uint16_t number)//I see a lot of duplicated code in this function
{
    if(number < 10)
    {
        int16_t n = convert_to_seven_segemnt((int8_t)number);
        n |= 1<<2;  // led 1 on
        spi2_send_word(n);
    }
    else if ((number >= 10) && (number < 100))
    {
        int16_t t = convert_to_seven_segemnt((int8_t)number/10);
        t |= 1<<2;  // led 1 on
        spi2_send_word(t); 
        delay_msec(wait_time);

        int16_t n = convert_to_seven_segemnt((int8_t)number%10);
        n |= 1<<5;  // led 2 on
        spi2_send_word(n);  
        delay_msec(wait_time); 
    }
     else if ((number >= 100) && (number < 1000))
    {
        int16_t h = convert_to_seven_segemnt((int8_t)(number/100));
        h |= 1<<2;  // led 1 on
        spi2_send_word(h); 
        delay_msec(wait_time);

        int16_t t = convert_to_seven_segemnt((int8_t)((number/10)%10));
        t |= 1<<5;  // led 1 on
        spi2_send_word(t); 
        delay_msec(wait_time);

        int16_t n = convert_to_seven_segemnt((int8_t)(number%10));
        n |= 1<<6;  // led 2 on
        spi2_send_word(n);  
        delay_msec(wait_time);   
    }
     else if ((number >= 1000) && (number < 10000))
    {
         int16_t s = convert_to_seven_segemnt((int8_t)(number/1000));
        s |= 1<<2;  // led 1 on
        spi2_send_word(s); 
        delay_msec(wait_time);

        int16_t h = convert_to_seven_segemnt((int8_t)((number/100)%10));
        h |= 1<<5;  // led 1 on
        spi2_send_word(h); 
        delay_msec(wait_time);

        int16_t t = convert_to_seven_segemnt((int8_t)((number/10)%10));
        t |= 1<<6;  // led 1 on
        spi2_send_word(t); 
        delay_msec(wait_time);

        int16_t n = convert_to_seven_segemnt((int8_t)(number%10));
        n |= 1<<1;  // led 2 on
        spi2_send_word(n);  
        delay_msec(wait_time);   
    }
}

void clear_lcd()
{
    spi2_send_word(0);  
}


#endif
