#include "stm32f4xx.h"
#include <string.h>

//  +-------------------------------------+
//  |                 GPIO                |
//  +-------------------------------------+
void gpio_init(void);
void delay_msec(int ms);
void delay_ns(int ns);

//  +-------------------------------------+
//  |                 UART                |
//  +-------------------------------------+
// PG14 -> TX
void uart_write_byte(char byte);
void delay_115200_bode();
void uart_println_string();
void uart_print_string();
void uart_write_2_bytes(uint16_t);
void uart_print_number_until_999(uint16_t number);
void pause();

//  +-------------------------------------+
//  |                 I2C                 |
//  +-------------------------------------+
/*
 * SCL ->  D15 -> PB8
 * SDA ->  D14 -> PB9
 */
#define SCL_LOW()    GPIOB->BSRR = GPIO_BSRR_BR_8
#define SCL_HIGH()   GPIOB->BSRR = GPIO_BSRR_BS_8
#define SDA_LOW()    GPIOB->BSRR = GPIO_BSRR_BR_9
#define SDA_HIGH()   GPIOB->BSRR = GPIO_BSRR_BS_9

void i2c_start();
int8_t i2c_send_byte(uint8_t byte);
int8_t i2c_read_adress(uint16_t adress);
uint8_t i2c_write_adress(uint16_t adress, int8_t data);

//  +-------------------------------------+
//  |                 MAIN                |
//  +-------------------------------------+

int main(void)
{
    //set_sysclk_max();

    gpio_init();
    delay_msec(300);

    uint16_t adress = 0;


    i2c_write_adress(288,83);
    delay_msec(100);
    i2c_write_adress(289,116);
    delay_msec(100);
    i2c_write_adress(290,101);
    delay_msec(100);
    i2c_write_adress(291,112);
    delay_msec(100);
    i2c_write_adress(292, 97);
    delay_msec(100);
    i2c_write_adress(293,110);
    delay_msec(100);
    i2c_write_adress(294, 101);
    delay_msec(100);
    i2c_write_adress(295,110);
    delay_msec(100);
    i2c_write_adress(296,107);
    delay_msec(100);
    i2c_write_adress(297,111);
    delay_msec(100);

    uart_println_string(" m23c16 dump ");
    int8_t data = 0;
    while(adress < 2047)
    {
        data = i2c_read_adress(adress);
        uart_write_byte(data);
        adress += 1;
    }
    return 0;
}

//  +-------------------------------------+
//  |                 GPIO                |
//  +-------------------------------------+
void gpio_init(void)
{
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN
                  | RCC_AHB1ENR_GPIODEN
                  | RCC_AHB1ENR_GPIOGEN
                  | RCC_AHB1ENR_GPIOHEN;
    //TX
    GPIOG->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR14;
    GPIOG->PUPDR |= GPIO_PUPDR_PUPDR14_1;
    GPIOG->MODER |= GPIO_MODER_MODER14_0;

    // I2C
          // SCL
    GPIOB->MODER |= GPIO_MODER_MODER8_0;
    GPIOB->OTYPER |= (1 << 8);
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8;
    //GPIOB->PUPDR &= ~(3<<16);
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR8_0;
    GPIOB->ODR |= GPIO_ODR_ODR_8;

        // SDA
    GPIOB->MODER |= GPIO_MODER_MODER9_0;
    GPIOB->OTYPER |= (1 << 9);
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9;
    //GPIOB->PUPDR &= ~(3<<18);
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR9_0;
    GPIOB->ODR |= GPIO_ODR_ODR_9;
}

void delay_msec(int ms)
{
    for(int i = 0; i < 1000 * ms; ++i)
    {
        asm("nop");

    }
}

void delay_ns(int ns)
{
    for (int i = 0; i < ns; i++) {
        asm("nop");
    }
}

//  +-------------------------------------+
//  |                 I2C                 |
//  +-------------------------------------+
/*
 * SCL ->  D15 -> PB8
 * SDA ->  D14 -> PB9
 */
void i2c_start()
{
    uint_fast16_t time_ns = 1;
    SDA_HIGH();
    delay_ns(time_ns);
    SCL_HIGH();
    delay_ns(time_ns);
    SDA_LOW();
    delay_ns(time_ns);
    SCL_LOW();
    delay_ns(time_ns);
}

void i2c_stop()
{
    uint_fast16_t time_ns = 1;
    SDA_LOW();
    delay_ns(time_ns);
    SCL_HIGH();
    delay_ns(time_ns);
    SDA_HIGH();
}

uint8_t i2c_read_sda_status()
{
    if(GPIOB->IDR & (1 << 9))
        return 1;
    else
        return 0;
}

int8_t i2c_send_byte(uint8_t byte)
{
    uint_fast16_t time_ns = 1;
    for (char i = 0; i < 8; i++)
    {
        if (byte & 0x80)
        {
            SDA_HIGH();
        }
        else
        {
            SDA_LOW();
        }
        delay_ns(time_ns);
        SCL_HIGH();
        delay_ns(time_ns);
        SCL_LOW();
        delay_ns(time_ns);
        byte = byte << 1;
    }
    delay_ns(time_ns);
    SCL_HIGH();
    delay_ns(time_ns);

    if(i2c_read_sda_status())
    {
        //uart_println_string("NACK");
        SCL_LOW();
        delay_ns(time_ns);
        return 1;
    }
    else
    {
        //uart_println_string("ACK");
        SCL_LOW();
        delay_ns(time_ns);
        return 0;
    }
}

int8_t i2c_read_adress(uint16_t adress)
{
    uint8_t h_adr = adress >> 8;
    h_adr = 0x07 & h_adr;
    h_adr = h_adr << 1;
    uint8_t l_adr = (uint8_t)adress;
    uint_fast16_t time_ns = 1;
    i2c_start();
    uint8_t instruction = 0xA0 | h_adr;     // Device select code 1010 + h_adr(A10-A8) + Write
    i2c_send_byte(instruction);

    i2c_send_byte(l_adr);                  //  l_adress(A7-A0)

    i2c_start();                            // start repeat

    instruction = 0xA1 | h_adr;             // Device select code 1010 + h_adr(A10-A8) + Read
    i2c_send_byte(instruction);

    int8_t data = 0;
    for (char i = 0; i < 9; i++)
    {
        SCL_HIGH();
        delay_ns(time_ns);
        if(i2c_read_sda_status() && i < 8)
            data |= 1 << (7-i);
        SCL_LOW();
        delay_ns(time_ns);
    }

    i2c_stop();

    return data;
}

uint8_t i2c_write_adress(uint16_t adress, int8_t data)
{
     uint8_t h_adr = adress >> 8;
    h_adr = 0x07 & h_adr;
    h_adr = h_adr << 1;
    uint8_t l_adr = (uint8_t)adress;
    uint_fast16_t time_ns = 1;
    i2c_start();
    uint8_t instruction = 0xA0 | h_adr;    // Device select code 1010 + h_adr(A10-A8) + Write
    i2c_send_byte(instruction);

    i2c_send_byte(l_adr);                  //  l_adress(A7-A0)
    i2c_send_byte(data);
    i2c_stop();
    return 0;

}

//  +-------------------------------------+
//  |                 UART                |
//  +-------------------------------------+

void uart_print_string(char* message)
{
    uint16_t size = strlen (message);
    GPIOG->BSRR = GPIO_BSRR_BS_14;
    for (char i = 0; i < size; ++i)
    {
        uart_write_byte(message[i]);
        delay_msec(50);
    }
    pause();

}

void uart_println_string(char* message)
{
    uart_print_string(message);
    uart_write_byte('\n');
    uart_write_byte('\r');
    pause();
}

void uart_write_byte(char byte)
{
    GPIOG->BSRR = GPIO_BSRR_BS_14;
    delay_115200_bode();
    GPIOG->BSRR = GPIO_BSRR_BR_14;
    delay_115200_bode();

    for (char i = 0; i < 8; i++)
    {
        if (byte & 0b00000001)
        {
            GPIOG->BSRR = GPIO_BSRR_BS_14;
        }
        else
        {
            GPIOG->BSRR = GPIO_BSRR_BR_14;
        }
        delay_115200_bode();
        byte>>=1;
    }
    GPIOG->BSRR = GPIO_BSRR_BS_14;
    delay_115200_bode();
}

void uart_print_number_until_999(uint16_t number)
{
    if(number < 10)
    {
        uart_write_byte(number + 0x30);
        uart_write_byte(' ');
        uart_write_byte(' ');
        //send_byte('\n'); send_byte('\r');
    }
    else if ((number >= 10) && (number < 100))
    {
        uart_write_byte(number/10 + 0x30);
        uart_write_byte(number%10 + 0x30);
        uart_write_byte(' ');
        //send_byte('\n'); send_byte('\r');
    }
     else if ((number >= 100) && (number < 1000))
    {
        uart_write_byte(number/100 + 0x30);
        uart_write_byte((number/10)%10 + 0x30);
        uart_write_byte(number%10 + 0x30);
        //send_byte('\n'); send_byte('\r');
    }
}

void uart_write_2_bytes(uint16_t data)
{
    uart_write_byte(data >> 8);
    uart_write_byte(data);
}

void delay_115200_bode()
{
   float x = 8.0; //int x = 80
    while(x < 10.0)// (x < 100)
    {
        x = x + 0.2f; // x += 2 Why do you need float?
        asm("nop"); // I supposed with float I can get more accurate delay.
    }
}

void pause()
{
    for(int i = 0; i < 90000; i++)
    {
        asm("nop");
    }
}

