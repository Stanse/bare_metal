#ifndef DELAY_H
#define DELAY_H

void delay_msec(int ms)   
{
    for(int i = 0; i < 1000 * ms; ++i)
    {
        asm("nop");
    }
}

void delay_ns(int ns)
{
    for (int i = 0; i < ns; i++) {
        asm("nop");
    }
}

#endif