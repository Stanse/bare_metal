#ifndef SPI_2_H 

#define SPI_2_H 

#include "stm32f4xx.h"
#include <stdint.h>
#include "delay.h"
#include "uart.h"


//  +-------------------------------------+
//  |                 SPI                 |
//  +-------------------------------------+
/* 
 * MOSI ->  D11 -> PB15
 * MISO ->  D12 -> PB14
 * CLK  ->  D13 -> PD3
 * RCLK ->  D8  -> PG10
 * CS   ->  D10 -> PH6
 */

#define CS_LOW()    GPIOH->BSRR = GPIO_BSRR_BR_6
#define CS_HIGH()   GPIOH->BSRR = GPIO_BSRR_BS_6

void spi2_init(void)
{
    RCC->AHB1ENR  |= RCC_AHB1ENR_GPIOBEN
                  | RCC_AHB1ENR_GPIODEN
                  | RCC_AHB1ENR_GPIOGEN
                  | RCC_AHB1ENR_GPIOHEN; 

    // MOSI
    GPIOB->MODER   |= GPIO_MODER_MODER15_1;
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15;
    GPIOB->AFR[1]  |= (5 << 28);    
   
    // MISO
    GPIOB->MODER   |= GPIO_MODER_MODER14_1;             
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR14;
    GPIOB->AFR[1]  |= (5 << 24);  

    // SCK
    GPIOD->MODER   |= GPIO_MODER_MODER3_1;
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR3;
    GPIOD->AFR[0]  |= (5 << 12);
       
    // RCLK
    GPIOG->MODER   |= GPIO_MODER_MODER10_0;
    GPIOG->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR10;

    // CS
    GPIOH->MODER |= GPIO_MODER_MODER6_0;
    GPIOH->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR6;


    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;

    SPI2->CR1 = 0;
    //SPI2->CR1  |= SPI_CR1_BIDIMODE;  // 15. 1: 1-line bidirectional data mode selected
    //SPI2->CR1  |= SPI_CR1_BIDIOE;    // 14. 1: Output enabled (transmit-only mode)
    //SPI2->CR1  |= SPI_CR1_CRCEN;     // 13. 1: CRC calculation enabled
    //SPI2->CR1  |= SPI_CR1_CRCNEXT;   // 12. 1: Next transfer is CRC (CRC phase)
    SPI2->CR1  |= SPI_CR1_DFF;       // 11. 1: 16-bit data frame format is selected for transmission/reception
    //SPI2->CR1  |= SPI_CR1_RXONLY;    // 10. 1: Output disabled (Receive-only mode)
    SPI2->CR1  |= SPI_CR1_SSM;       // 9.  1: Software slave management enabled
    //SPI2->CR1  |= SPI_CR1_SSI;       // 8.  Internal slave select
    SPI2->CR1 &= ~SPI_CR1_LSBFIRST;  // 7.  1: LSB transmitted first
    SPI2->CR1  |= SPI_CR1_BR;        // 5:3 100: f_PCLK/32
    SPI2->CR1  |= SPI_CR1_MSTR;      // 2.  1: Master configuration
    SPI2->CR1  &= ~SPI_CR1_CPOL;       // 1.  1: CK to 1 when idle
    SPI2->CR1  &= ~SPI_CR1_CPHA;          // 0.  1: The second clock transition is the first data capture edge  
    //SPI2->CR1  |= SPI_CR1_CPHA;

    //NVIC_EnableIRQ(SPI2_IRQn);
    SPI2->CR2 = 0;
   // SPI2->CR2 |= 1 << 4;
    SPI2->CR2 = SPI_CR2_SSOE |SPI_CR2_RXNEIE;
   // SPI2->CR2 |= SPI_CR2_TXEIE;
   // SPI2->CR2 |= SPI_CR2_RXNEIE;
    
    SPI2->CR1  |= SPI_CR1_SPE;       // 6.  1: SPI ENABLE
}

void spi2_send_word(int16_t word)
{
    while((SPI2->SR & SPI_SR_TXE) == 0); // tx buf not empty
    CS_HIGH();
    SPI2->DR = word;
    while (SPI2->SR & SPI_SR_BSY);
    GPIOG->BSRR |= GPIO_BSRR_BS_10;
    GPIOG->BSRR |= GPIO_BSRR_BR_10;  
    CS_LOW();
}

uint16_t spi2_read_eeprom(uint16_t adress)
{   
    uint16_t data = 0;
    uint8_t byte_counter = 0;
    while(!(SPI2->SR & SPI_SR_TXE)); // tx buf not empty
    CS_HIGH();
    adress = adress * 2;
    SPI2->DR = 0x3000 | adress;
    
    while(!(SPI2->SR & SPI_SR_RXNE));

    while (byte_counter < 3)
    {
        while(!(SPI2->SR & SPI_SR_TXE)); // tx buf not empty
        while (SPI2->SR & SPI_SR_BSY);

        SPI2->DR = 0x0000;
        
        while(!(SPI2->SR & SPI_SR_RXNE));
        
        data = READ_REG(SPI2->DR); 

        if(byte_counter == 2)
        {    
            CS_LOW(); 
            return data;
        }   
           
        while (SPI2->SR & SPI_SR_BSY);
        delay_msec(1);   
        byte_counter = byte_counter + 2;    
   } 
   return 0;   
}

void spi2_read_all_eeprom()
{
    uint16_t data = 0;
    uint16_t byte_counter = 0;  
    while(!(SPI2->SR & SPI_SR_TXE)); // tx buf not empty
    CS_HIGH();
    SPI2->DR = 0x3000;
    
    while(!(SPI2->SR & SPI_SR_RXNE));

    while (byte_counter < 1024)
    {
        while(!(SPI2->SR & SPI_SR_TXE)); // tx buf not empty
        while (SPI2->SR & SPI_SR_BSY);

        SPI2->DR = 0x0000;
        
        while(!(SPI2->SR & SPI_SR_RXNE));
        
        data = READ_REG(SPI2->DR); 
        uart3_write_2byte(data); 
        while (SPI2->SR & SPI_SR_BSY);

        byte_counter += 2;        
    }  
    CS_LOW();
}

void spi2_write_eeprom(uint16_t adress, uint16_t data)
{
    while(!(SPI2->SR & SPI_SR_TXE));       // tx buf not empty
    CS_HIGH();
    SPI2->DR = 0x1300;
    while(!(SPI2->SR & SPI_SR_RXNE));      // write enable
    while (SPI2->SR & SPI_SR_BSY);
    CS_LOW();

    CS_HIGH();    
    SPI2->DR = 0x1400 | adress;
    while(!(SPI2->SR & SPI_SR_RXNE));      // send adress
    while(!(SPI2->SR & SPI_SR_TXE));
    while (SPI2->SR & SPI_SR_BSY);
    SPI2->DR = data;                       // write data

    while(!(SPI2->SR & SPI_SR_RXNE));
    while (SPI2->SR & SPI_SR_BSY);
    CS_LOW();

    CS_HIGH();                              // CHECK
    CS_LOW();                   

    /*
    while(!(SPI2->SR & SPI_SR_TXE));        // write disable
    CS_HIGH(); 
    SPI2->DR = 0x1000; 
    while(!(SPI2->SR & SPI_SR_TXE));              
    while(!(SPI2->SR & SPI_SR_RXNE));
    while (SPI2->SR & SPI_SR_BSY); 
    delay_ms(3);
    CS_LOW();
    delay_ns(200);
    */
}

#endif
