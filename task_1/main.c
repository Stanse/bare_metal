#include "stm32f4xx.h"

void port_init();
void delay_ns(int ns);
void set_data_to_out();
void clear_lcd(void);
void SPI_write_two_byte(uint16_t data);
void set_led(uint16_t num_of_btn, uint16_t symbol);

int time_delay = 1;     // ns

//|==================  TABLE BITS OF 75HC595  =================+
//| Q | '15'| '14'| '13'| '12'| '11'| '10'| '9' | '8' |     |HI|
//|---+-----+-----+-----+-----+-----+-----+-----+-----+--------|
//|   |  G  |  C  |  Dp |  D  |  E  |Btn_4|Btn_3|Btn_2|        |
//| 0b|  1  |  0  |  0  |  1  |  1  |  1  |  0  |  0  |     "2"|
//|============================================================+
//| Q | '7' | '6' | '5' | '4' | '3' | '2' | '1' | '0' |     |LO|
//|---+-----+-----+-----+-----+-----+-----+-----+-----+--------|
//|   |  B  |Led_3|Led_2|  F  |  A  |Led_1|Led_4|Btn_1|        |
//| 0b|  1  |  0  |  0  |  0  |  1  |  1  |  0  |  0  |     "2"|
//|============================================================|
int main(void)
{
    port_init();

    clear_lcd();
    while (1)
    {
        //      BTN     LED  |  A   |  B   |   C   |   D   |   E   |   F  |   G   |
        set_led(1<<10, (1<<2)|(0<<3)|(1<<7)|(1<<14)|(0<<12)|(0<<11)|(0<<4)|(0<<15));   // "1"                             // "1"
        set_led(1<<9,  (1<<5)|(1<<3)|(1<<7)|(0<<14)|(1<<12)|(1<<11)|(0<<4)|(1<<15));   // "2"
        set_led(1<<8,  (1<<6)|(1<<3)|(1<<7)|(1<<14)|(1<<12)|(0<<11)|(0<<4)|(1<<15));   // "3"
        set_led(1<<0,  (1<<1)|(0<<3)|(1<<7)|(1<<14)|(0<<12)|(0<<11)|(1<<4)|(1<<15));   // "4"
    }
    return 0;
}

void set_led(uint16_t num_of_btn, uint16_t symbol)
{
    SPI_write_two_byte(num_of_btn);                     // "HIGHT" for check
    if(GPIOG->IDR & GPIO_IDR_IDR_11)                    // check Button
    {
        clear_lcd();
    }
    else
    {
       SPI_write_two_byte(symbol);
    }
}

void delay_ns(int ns)
{
    for(int i = 0; i < ns; i++)
    {
        asm("nop");
    }
}

void set_data_to_out()
{
    GPIOG->BSRR = GPIO_BSRR_BS_10;
    delay_ns(time_delay);
    GPIOG->BSRR = GPIO_BSRR_BR_10;
}

void SPI_write_two_byte(uint16_t data)
{
    char i;
    for (i = 0; i < 16; i++)
    {
        delay_ns(time_delay);
        if (data & 0x8000)
        {
            GPIOB->BSRR = GPIO_BSRR_BS_15;
        }
        else
        {
            GPIOB->BSRR = GPIO_BSRR_BR_15;
        }
        data<<=1;
        delay_ns(time_delay);
        GPIOD->BSRR = GPIO_BSRR_BS_3;
        delay_ns(time_delay);
        GPIOD->BSRR = GPIO_BSRR_BR_3;
    }
    set_data_to_out();

}

void clear_lcd()
{
    SPI_write_two_byte(0);
    set_data_to_out();
}

void port_init()
{
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;    // Port D ON
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;    // Port B ON
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;    // Port G ON

    GPIOD->OTYPER = 0;
    GPIOB->OTYPER = 0;
    GPIOG->OTYPER = 0;

    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR3_1;
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15_1;
    GPIOG->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR10_1;

    GPIOD->PUPDR |= GPIO_PUPDR_PUPDR3_1;
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR15_1;
    GPIOG->PUPDR |= GPIO_PUPDR_PUPDR10_1;

    // PROGRAM SPI INIT
    GPIOB->MODER |= GPIO_MODER_MODER15_0;     // DATA   (D11)
    GPIOD->MODER |= GPIO_MODER_MODER3_0;      // (D13)(SHCP = shift register clock input)
    GPIOG->MODER |= GPIO_MODER_MODER10_0;     // (D8) (STCP = STORAGE REG CLOCK )

    GPIOG->MODER &= ~GPIO_MODER_MODER11;      //(D7) KEY
}