#include "stm32f4xx.h"
#include "util.h"

void port_init();
void delay_115200_bode();
void send_byte(char byte);
void pause();

int main(void)
{
    port_init();
    char message[] = "Stepanenko A. Task 2 is done.";
    char size = sizeof(message) / sizeof(message[0]);
    GPIOG->BSRR = GPIO_BSRR_BS_14;
    while (1)
    {
        for (char i = 0; i < size; ++i)
        {
            send_byte(message[i]);
            pause();
        }
       send_byte('\n');
       send_byte('\r');
       pause();
    }
    return 0;
}

void send_byte(char byte)
{
    char i; //Move
    GPIOG->BSRR = GPIO_BSRR_BS_14;
    delay_115200_bode();
    GPIOG->BSRR = GPIO_BSRR_BR_14;
    delay_115200_bode();
   
    for (i = 0; i < 8; i++) // for (char i = 0; i < 8; i++) do like this since you don't need "i" somewhere else
    {
        if (byte & 0b00000001)// it is just 1
        {
            GPIOG->BSRR = GPIO_BSRR_BS_14;
            delay_115200_bode();//Move
        }
        else
        {
            GPIOG->BSRR = GPIO_BSRR_BR_14;
            delay_115200_bode(); //Move
        }
        //Nice place for delay_115200_bode
        byte>>=1;
    }
    GPIOG->BSRR = GPIO_BSRR_BS_14;
    delay_115200_bode();
}

void delay_115200_bode()
{
    float x = 8.0; //int x = 80
    while(x < 10.0)// (x < 100)
    {
        x = x + 0.2f; // x += 2 Why do you need float?
        asm("nop");
    }
}

void pause()
{
    for(int i = 0; i < 90000; i++)
    {
        asm("nop");
    }
}

void port_init()
{
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;            // PG14 -> TX
    GPIOG->OTYPER = 0;
    GPIOG->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR14;  
    GPIOG->PUPDR |= GPIO_PUPDR_PUPDR14_1;         
    GPIOG->MODER |= GPIO_MODER_MODER14_0;
}

