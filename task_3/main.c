#include "stm32f4xx.h"
#include "util.h"
#include <string.h>

//  +-------------------------------------+
//  |                 GPIO                |
//  +-------------------------------------+
void gpio_init(void);
void delay_ms(int ms);
void delay_ns(int ns);

//  +-------------------------------------+
//  |                 UART                |
//  +-------------------------------------+
// PG14 -> TX
void uart_write_byte(char byte);
void delay_115200_bode();
void uart_println_string();
void uart_print_string();
void uart_write_2_bytes(uint16_t);
void uart_print_number_until_999(uint16_t number);
void pause();

//  +-------------------------------------+
//  |                 SPI                 |
//  +-------------------------------------+
/*
 * MOSI ->  D11 -> PB15
 * MISO ->  D12 -> PB14
 * CLK  ->  D13 -> PD3
 * RCLK ->  D8  -> PG10
 * CS   ->  D10 -> PH6
 */

#define CS_LOW()    GPIOH->BSRR = GPIO_BSRR_BR_6
#define CS_HIGH()   GPIOH->BSRR = GPIO_BSRR_BS_6

void spi2_init(void);
void spi2_send_word(uint16_t word);
uint16_t spi2_read_eeprom(uint16_t adress);
void spi2_read_all_eeprom();
void spi2_write_eeprom(uint16_t adress, uint16_t data);

//  +-------------------------------------+
//  |                 MAIN                |
//  +-------------------------------------+

int main(void)
{
    //set_sysclk_max();

    gpio_init();
    spi2_init();
    CS_LOW();
    uint16_t ms_to_delay = 50;
    delay_ms(3);

    ///*
    spi2_write_eeprom(144, 0x5374);
    delay_ms(ms_to_delay);
    spi2_write_eeprom(145,  0x6570);
    delay_ms(ms_to_delay);
    spi2_write_eeprom(146,  0x616E);
    delay_ms(ms_to_delay);
    spi2_write_eeprom(147,  0x656E);
    delay_ms(ms_to_delay);
    spi2_write_eeprom(148,  0x6B6F);
    delay_ms(ms_to_delay);
    //*/

    spi2_read_all_eeprom();

    return 0;
}

//  +-------------------------------------+
//  |                 GPIO                |
//  +-------------------------------------+
void gpio_init(void)
{
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN
                  | RCC_AHB1ENR_GPIODEN
                  | RCC_AHB1ENR_GPIOGEN
                  | RCC_AHB1ENR_GPIOHEN;
    //TX
    GPIOG->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR14;
    GPIOG->PUPDR |= GPIO_PUPDR_PUPDR14_1;
    GPIOG->MODER |= GPIO_MODER_MODER14_0;
    GPIOG->BSRR = GPIO_BSRR_BS_14;

    // MOSI
    GPIOB->MODER   |= GPIO_MODER_MODER15_1;
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15;
    GPIOB->AFR[1]  |= (5 << 28);

    // MISO
    GPIOB->MODER   |= GPIO_MODER_MODER14_1;
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR14;
    GPIOB->AFR[1]  |= (5 << 24);

    // SCK
    GPIOD->MODER   |= GPIO_MODER_MODER3_1;
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR3;
    GPIOD->AFR[0]  |= (5 << 12);

    // RCLK
    GPIOG->MODER   |= GPIO_MODER_MODER10_0;
    GPIOG->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR10;

    // CS
    GPIOH->MODER |= GPIO_MODER_MODER6_0;
    GPIOH->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR6;
}

void delay_ms(int ms)
{
    for(int i = 0; i < 1000 * ms; ++i)
    {
        asm("nop");

    }
}

void delay_ns(int ns)
{
    for (int i = 0; i < ns; i++) {
        asm("nop");
    }
}

//  +-------------------------------------+
//  |                 SPI                 |
//  +-------------------------------------+
void spi2_init(void)
{
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;

    SPI2->CR1 = 0;
    //SPI2->CR1  |= SPI_CR1_BIDIMODE;  // 15. 1: 1-line bidirectional data mode selected
    //SPI2->CR1  |= SPI_CR1_BIDIOE;    // 14. 1: Output enabled (transmit-only mode)
    //SPI2->CR1  |= SPI_CR1_CRCEN;     // 13. 1: CRC calculation enabled
    //SPI2->CR1  |= SPI_CR1_CRCNEXT;   // 12. 1: Next transfer is CRC (CRC phase)
    SPI2->CR1  |= SPI_CR1_DFF;         // 11. 1: 16-bit data frame format is selected for transmission/reception
    //SPI2->CR1  |= SPI_CR1_RXONLY;    // 10. 1: Output disabled (Receive-only mode)
    SPI2->CR1  |= SPI_CR1_SSM;         // 9.  1: Software slave management enabled
    //SPI2->CR1  |= SPI_CR1_SSI;       // 8.  Internal slave select
    SPI2->CR1 &= ~SPI_CR1_LSBFIRST;    // 7.  1: LSB transmitted first
    SPI2->CR1  |= SPI_CR1_BR;          // 5:3 100: f_PCLK/32
    SPI2->CR1  |= SPI_CR1_MSTR;        // 2.  1: Master configuration
    SPI2->CR1  &= ~SPI_CR1_CPOL;       // 1.  1: CK to 1 when idle
    SPI2->CR1  &= ~SPI_CR1_CPHA;       // 0.  1: The second clock transition is the first data capture edge
    //SPI2->CR1  |= SPI_CR1_CPHA;

    SPI2->CR2 = 0;
    SPI2->CR2 = SPI_CR2_SSOE |SPI_CR2_RXNEIE | SPI_CR2_TXEIE;

    SPI2->CR1  |= SPI_CR1_SPE;       // 6.  1: SPI ENABLE
}

void spi2_send_word(uint16_t word)
{
    while((SPI2->SR & SPI_SR_TXE) == 0){}         // tx buf not empty
    CS_HIGH();
    SPI2->DR = word;
    while(!(SPI2->SR & SPI_SR_RXNE));
    while (SPI2->SR & SPI_SR_BSY);
    CS_LOW();
    delay_ms(1);
}


uint16_t spi2_read_eeprom(uint16_t adress)
{
    uint16_t data = 0;
    uint8_t byte_counter = 0;
    while(!(SPI2->SR & SPI_SR_TXE));            // tx buf not empty
    CS_HIGH();
    adress = adress * 2;
    SPI2->DR = 0x3000 | adress;

    while(!(SPI2->SR & SPI_SR_RXNE));

    while (byte_counter < 3)
    {
        while(!(SPI2->SR & SPI_SR_TXE));       // tx buf not empty
        while (SPI2->SR & SPI_SR_BSY);

        SPI2->DR = 0x0000;

        while(!(SPI2->SR & SPI_SR_RXNE));

        data = READ_REG(SPI2->DR);

        if(byte_counter == 2)
        {
            CS_LOW();
            return data;
        }

        while (SPI2->SR & SPI_SR_BSY);
        byte_counter = byte_counter + 2;
   }
   return 0;
}

void spi2_read_all_eeprom()
{
    uint16_t data = 0;
    uint16_t byte_counter = 0;
    while(!(SPI2->SR & SPI_SR_TXE));           // tx buf not empty
    CS_HIGH();
    SPI2->DR = 0x3000;

    while(!(SPI2->SR & SPI_SR_RXNE));

    while (byte_counter < 1024)
    {
        while(!(SPI2->SR & SPI_SR_TXE));       // tx buf not empty
        while (SPI2->SR & SPI_SR_BSY);

        SPI2->DR = 0x0000;

        while(!(SPI2->SR & SPI_SR_RXNE));

        data = READ_REG(SPI2->DR);

        uart_write_2_bytes(data);

        while (SPI2->SR & SPI_SR_BSY);

        byte_counter += 2;
    }
    CS_LOW();
}

void spi2_write_eeprom(uint16_t adress, uint16_t data)
{
    uint16_t ms_to_delay = 50;
    while(!(SPI2->SR & SPI_SR_TXE));            // tx buf not empty
    CS_HIGH();
    SPI2->DR = 0x1300;
    while(!(SPI2->SR & SPI_SR_RXNE));           // write enable
    while (SPI2->SR & SPI_SR_BSY);
    CS_LOW();
    delay_ms(ms_to_delay);

    CS_HIGH();
    SPI2->DR = 0x1400 | adress;
    while(!(SPI2->SR & SPI_SR_RXNE));           // send adress
    while(!(SPI2->SR & SPI_SR_TXE));
    while (SPI2->SR & SPI_SR_BSY);
    SPI2->DR = data;                            // write data

    while(!(SPI2->SR & SPI_SR_RXNE));
    while (SPI2->SR & SPI_SR_BSY);
    CS_LOW();
    delay_ms(ms_to_delay);

    CS_HIGH();                                  // CHECK
    delay_ms(ms_to_delay);
    CS_LOW();
    delay_ns(ms_to_delay);

    /*
    while(!(SPI2->SR & SPI_SR_TXE));            // write disable
    CS_HIGH();
    SPI2->DR = 0x1000;
    while(!(SPI2->SR & SPI_SR_TXE));
    while(!(SPI2->SR & SPI_SR_RXNE));
    while (SPI2->SR & SPI_SR_BSY);
    delay_ms(3);
    CS_LOW();
    delay_ns(200);
    */
}

//  +-------------------------------------+
//  |                 UART                |
//  +-------------------------------------+

void uart_print_string(char* message)
{
    uint16_t size = strlen (message);
    GPIOG->BSRR = GPIO_BSRR_BS_14;
    for (char i = 0; i < size; ++i)
    {
        uart_write_byte(message[i]);
        delay_ms(50);
    }
    pause();

}

void uart_println_string(char* message)
{
    uart_print_string(message);
    uart_write_byte('\n');
    uart_write_byte('\r');
    pause();
}

void uart_write_byte(char byte)
{
    GPIOG->BSRR = GPIO_BSRR_BR_14;
    delay_115200_bode();

    for (char i = 0; i < 8; i++)
    {
        if (byte & 0b00000001)
        {
            GPIOG->BSRR = GPIO_BSRR_BS_14;
        }
        else
        {
            GPIOG->BSRR = GPIO_BSRR_BR_14;
        }
        delay_115200_bode();
        byte >>= 1;
    }
    GPIOG->BSRR = GPIO_BSRR_BS_14;
    delay_115200_bode();
}

void uart_print_number_until_999(uint16_t number)
{
    if(number < 10)
    {
        uart_write_byte(number + 0x30);
        uart_write_byte(' ');
        uart_write_byte(' ');
    }
    else if ((number >= 10) && (number < 100))
    {
        uart_write_byte(number/10 + 0x30);
        uart_write_byte(number%10 + 0x30);
        uart_write_byte(' ');
    }
     else if ((number >= 100) && (number < 1000))
    {
        uart_write_byte(number/100 + 0x30);
        uart_write_byte((number/10)%10 + 0x30);
        uart_write_byte(number%10 + 0x30);
    }
}

void uart_write_2_bytes(uint16_t data)
{
    uart_write_byte(data >> 8);
    uart_write_byte(data);
}

void delay_115200_bode()
{
    float x = 8.0; //int x = 80
    while(x < 10.0)// (x < 100)
    {
        x = x + 0.2f; // x += 2 Why do you need float?
        asm("nop"); // I supposed with float I can get more accurate delay.
    }

}

void pause()
{
    for(int i = 0; i < 90000; i++)
    {
        asm("nop");
    }
}

