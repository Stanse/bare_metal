#include "stm32f4xx.h"
#include <string.h>
#include "rcc.h"
#include "i2c.h"
#include "spi_2.h"
#include "uart.h"
#include "delay.h"
#include "seven_seg.h"
#include <math.h>
	
#include <stdio.h>
#include <stdlib.h>

//  +-------------------------------------+
//  |                 GPIO                |
//  +-------------------------------------+
#define BUZZER_LOW()    GPIOG->BSRR = GPIO_BSRR_BR_13
#define BUZZER_HIGHT()   GPIOG->BSRR = GPIO_BSRR_BS_13

void gpio_init(void);

char* int_to_char_array(int16_t number);
//int16_t char_arr_to_int(char* str);

//  +-------------------------------------+
//  |                 MAIN                |
//  +-------------------------------------+
int16_t counter = 10000;
uint8_t* numberArray = NULL;
uint8_t on_pause = 0;
uint8_t btn_was_pressed = 0;
uint32_t tmpreg;
uint32_t SysTick_CNT = 0;

char data[32] = {0};
int8_t volatile  data_index = 0;//variable which is used in isr must be volatile
uint8_t menu[2] = {1, 2};
uint8_t task = 0;
uint8_t volatile  end_of_read = 0;//variable which is used in isr must be volatile
uint8_t setup_mode = 0;
uint8_t buzzer_is_on = 0;

uint8_t apply_settings = 0;
	
//void USART3_IRQHandler(void);//Do not need to do it. It is done in CMSIS
void decrement_counter(int16_t* p_counter);
//void SysTick_Handler(void);//Do not need to do it. It is done in CMSIS
void chek_buttons();

int main(void)
{
    rcc_reset();
    rcc_init();//Why you do not check return value here? -> I forgot about return value
    SysTick_Init();
    gpio_init(); 
    i2c_init();
    //uart_6_init();       // Do you use UART6 somewhere? -> I used uart6 for debugging.
    uart_3_init();         // And I read the task inattentively and used uart6 for write EEPROM in the first time.
    spi2_init(); 

    clear_lcd(); 

    show_number(counter);
    BUZZER_LOW();
    //on_pause = 1;
    
    if(!task)
    {
        uart3_write_str("\n\r");
        uart3_write_str("+------------------------------------------------+\n\r");
        uart3_write_str("|              BARE METAL. Task #5               |\n\r");
        uart3_write_str("+------------------------------------------------+\n\r");
        uart3_write_str("| 1. Set counter                                 |\n\r");
        uart3_write_str("| 2. Load from EEPROM                            |\n\r");
        uart3_write_str("+------------------------------------------------+\n\r");
    }
    
    while(1)
	{   
        if(!on_pause)
        {
            show_number(counter);
            chek_buttons();
        }
        while(buzzer_is_on)
        {
            BUZZER_HIGHT();
            delay_msec(1);
            BUZZER_LOW();
            delay_msec(1);
            show_number(0);
            chek_buttons();  
        }
        
           
       
        if(end_of_read)
        {          
            if(setup_mode)
            {
                int8_t tmp = 0;

                switch (task)
                {
                case 1:                             // set counter
                    counter = atoi(data);
                    uart3_write_str("counter = ");
                    uart3_write_str(data);
                    //uart3_write_str('\n');

                    i2c_write_adress(0, counter>>8);
                    delay_msec(200);
                    i2c_write_adress(1, counter);
                    delay_msec(200);

                    setup_mode = 0;
                    on_pause = 0;
                    data_index = 0;
                    break;
                
                case 2:                             // read counter from EEPROM            
                    tmp = i2c_read_adress(0);
                    counter = tmp<<8;
                    tmp = i2c_read_adress(1);
                    counter |= tmp;
                    uart3_write_str("counter = ");
                    numberArray = int_to_char_array(counter);
                    uart3_write_str(numberArray);                       //memory leak
                    free(numberArray);                                  // fixed memory leak

                    setup_mode = 0;
                    on_pause = 0;
                    data_index = 0;
                    break;

                default:
                    data_index = 0;
                    end_of_read = 0;
                    task = 0;
                    break;
                }
            }

            if(task == 0)
            {   
                task = data[0] - 0x30;
                switch (task)
                {
                case 1:
                    uart3_write_str("Input value of counter:\n\r");
                    uart3_write_str("->");
                    setup_mode = 1;
                    data_index = 0;
                    data[0] = 0;
                    end_of_read = 0;
                    break;
                case 2:
                    uart3_write_str("Reading EEPROM:\n\r");
                    setup_mode = 1;
                    break;
                
                default:
                    break;
                }   
            } 
        }
    }    
    return 0;
}

//  +-------------------------------------+
//  |                 GPIO                |
//  +-------------------------------------+

void gpio_init(void)
{
    RCC->AHB1ENR    |= RCC_AHB1ENR_GPIOGEN;

    GPIOG->MODER    &= ~GPIO_MODER_MODER11;              //(D7) KEY

    GPIOG->MODER    |= GPIO_MODER_MODER13_0;             // PG13
    GPIOG->OSPEEDR  |= GPIO_OSPEEDER_OSPEEDR13; 
    GPIOG->OTYPER   &= ~(1 << 13); 
}


//-------------------------------------------
void decrement_counter(int16_t* p_counter)
{
    MODIFY_REG(SysTick->VAL,SysTick_VAL_CURRENT_Msk, 168000000 / 1000 - 1);
    SysTick_CNT = 1000;
    *p_counter += -1;//func name is "increment_" but it is decrement in fact. Isn't? -> Yes, you are right.
    if(*p_counter < 0)
    {
        on_pause = 1; 
        buzzer_is_on = 1;
        *p_counter = 10000;
    }
}

void SysTick_Handler(void)
{
    if(SysTick_CNT > 0)
        SysTick_CNT--;
    if(SysTick_CNT == 0 && on_pause == 0)
    {
        decrement_counter(&counter);
    } 
}


void chek_buttons()
{
    if(GPIOG->IDR & GPIO_IDR_IDR_11 && btn_was_pressed == 0)   
        {
            btn_was_pressed = 1;
            if(on_pause && btn_was_pressed)
            {
                on_pause = 0;
                buzzer_is_on = 0;
            }
            else if(on_pause == 0 && btn_was_pressed)
            {
                on_pause = 1;
                buzzer_is_on = 1;
            }
            delay_msec(200);  
        }
    if((GPIOG->IDR & GPIO_IDR_IDR_11) == 0)
        btn_was_pressed = 0;
}

char* int_to_char_array(int16_t number)
{
    if(!number)
    {
        char* numberArray = calloc(2, sizeof(char));
        numberArray[0] = 0x30;
        numberArray[1] = 0x0A;
    }
    else
    {    
        char n = log10(number) + 2;//Why do you need a log10()? log10() returns a double actually.
        char* numberArray = calloc(n, sizeof(char));
        for (char i = 0; i < n - 1; i++)
        {
            numberArray[n-2-i] = (char)(number % 10) + 0x30;//What happens when n < 0?
            number /= 10;                                   //we will read the data at (&numberArray - n)
        }                                                   // it will never happen because char i = 0
        numberArray[n-1] = 0x0A;
    }
    return numberArray;      //right place for return numberArray;
}

void USART3_IRQHandler(void)
{
    
  if((READ_BIT(USART3->SR, USART_SR_RXNE) == (USART_SR_RXNE)) && (READ_BIT(USART3->CR1, USART_CR1_RXNEIE) == (USART_CR1_RXNEIE)))
  {
    data[data_index] = (uint8_t)(USART3->DR & 0x00FF);
   
    if(data[data_index]==0x0A)
    {
        end_of_read = 1;
    }
    data_index++;
  }
  else
  {
    if(READ_BIT(USART3->SR, USART_SR_ORE) == (USART_SR_ORE))
    {
      (void) USART3->DR;
    }
    else if(READ_BIT(USART3->SR, USART_SR_FE) == (USART_SR_FE))
    {
      (void) USART3->DR;
    }
    else if(READ_BIT(USART3->SR, USART_SR_ORE) == (USART_SR_ORE))
    {
      (void) USART3->DR;
    }
  }
}
