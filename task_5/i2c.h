#ifndef I2C_H
#define I2C_H

#include "stm32f4xx.h"
#include "delay.h"
//  +-------------------------------------+
//  |                 I2C                 |
//  +-------------------------------------+
/*
 * SCL ->  D15 -> PB8
 * SDA ->  D14 -> PB9
 */

#define SCL_LOW()    GPIOB->BSRR = GPIO_BSRR_BR_8
#define SCL_HIGH()   GPIOB->BSRR = GPIO_BSRR_BS_8
#define SDA_LOW()    GPIOB->BSRR = GPIO_BSRR_BR_9
#define SDA_HIGH()   GPIOB->BSRR = GPIO_BSRR_BS_9

void i2c_init()
{
    RCC->AHB1ENR  |= RCC_AHB1ENR_GPIOBEN;
    // SCL
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8; 
    GPIOB->PUPDR &= ~(3<<16); 
    GPIOB->OTYPER |= (1 << 8);       
    GPIOB->MODER |= GPIO_MODER_MODER8_0;

     // SDA
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9; 
    GPIOB->PUPDR &= ~(3<<18);
    GPIOB->OTYPER |= (1 << 9);           
    GPIOB->MODER |= GPIO_MODER_MODER9_0;
}

void i2c_start()
{
    uint16_t time_ns = 50;
    SDA_HIGH();
    delay_ns(time_ns);
    SCL_HIGH();
    delay_ns(time_ns);
    SDA_LOW();
    delay_ns(time_ns);
    SCL_LOW();
    delay_ns(time_ns);
}

void i2c_stop()
{
    uint16_t time_ns = 50;
    SDA_LOW();
    delay_ns(time_ns);
    SCL_HIGH();
    delay_ns(time_ns);
    SDA_HIGH();
}

uint8_t i2c_read_sda_status()
{
    if(GPIOB->IDR & (1 << 9))
        return 1;
    else
        return 0;
}

int8_t i2c_send_byte(uint8_t byte)
{
    uint16_t time_ns = 50;
    for (char i = 0; i < 8; i++)
    {
        if (byte & 0x80)
        {
            SDA_HIGH();
        }
        else
        {
            SDA_LOW();
        }
        delay_ns(time_ns);
        SCL_HIGH();
        delay_ns(time_ns);
        SCL_LOW();
        delay_ns(time_ns);
        byte = byte << 1;
    }
    delay_ns(time_ns);
    SCL_HIGH();
    delay_ns(time_ns);

    if(i2c_read_sda_status())
    {
        SCL_LOW();
        delay_ns(time_ns);
        return 1;
    }
    else
    {
        SCL_LOW();
        delay_ns(time_ns);
        return 0;
    }
}

int8_t i2c_read_adress(uint16_t adress)
{
    uint8_t h_adr = adress >> 8;
    h_adr = 0x07 & h_adr;
    h_adr = h_adr << 1;
    uint8_t l_adr = (uint8_t)adress;
    uint16_t time_ns = 50;
    i2c_start();
    uint8_t instruction = 0xA0 | h_adr;     // Device select code 1010 + h_adr(A10-A8) + Write
    i2c_send_byte(instruction);

    i2c_send_byte(l_adr);                  //  l_adress(A7-A0)

    i2c_start();                            // start repeat

    instruction = 0xA1 | h_adr;             // Device select code 1010 + h_adr(A10-A8) + Read
    i2c_send_byte(instruction);

    int8_t data = 0;
    for (char i = 0; i < 9; i++)
    {
        SCL_HIGH();
        delay_ns(time_ns);
        if(i2c_read_sda_status() && i < 8)
            data |= 1 << (7-i);
        SCL_LOW();
        delay_ns(time_ns);
    }

    i2c_stop();

    return data;
}

uint8_t i2c_write_adress(uint16_t adress, int8_t data)
{
    uint8_t h_adr = adress >> 8;
    h_adr = 0x07 & h_adr;
    h_adr = h_adr << 1;
    uint8_t l_adr = (uint8_t)adress;
    i2c_start();
    uint8_t instruction = 0xA0 | h_adr;    // Device select code 1010 + h_adr(A10-A8) + Write
    i2c_send_byte(instruction);

    i2c_send_byte(l_adr);                  //  l_adress(A7-A0)
    i2c_send_byte(data);
    i2c_stop();
    return 0;

}



#endif 